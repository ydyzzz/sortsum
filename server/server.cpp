#include "server.h"
#include "socketstream.h"
#include "handler.h"

#include <algorithm>
#include <functional>
#include <iostream>

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

sortsum::Server::Server(int port, size_t size, size_t max_buffer,
        std::function<bool(sortsum::SocketStream*)> handler) 
    : max_buffer(max_buffer), handler(handler) {
    pool.resize(size);

    tcp_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (tcp_socket < 0) {
        perror("socket() failed (tcp)");
        exit(-1);
    }

    udp_socket = socket(AF_INET, SOCK_DGRAM, 0);
    if (udp_socket < 0) {
        perror("socket() failed (udp)");
        exit(-1);
    }

    memset(&s_addr, 0, sizeof(s_addr));
    s_addr.sin_family = AF_INET;
    s_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    s_addr.sin_port = htons(port);

    if (bind(tcp_socket, (const struct sockaddr *) &s_addr, sizeof(s_addr)) < 0) {
        perror("bind() failed (tcp)");
        close(tcp_socket);
        close(udp_socket);
        exit(-1);
    }
    if (bind(udp_socket, (const struct sockaddr *) &s_addr, sizeof(s_addr)) < 0) {
        perror("bind() failed (udp)");
        close(tcp_socket);
        close(udp_socket);
        exit(-1);
    }

    int on = 1;
    if (setsockopt(tcp_socket, SOL_SOCKET, SO_REUSEADDR,
                (char *) &on, sizeof(on)) < 0) { 
        perror("setsockopt() failed (tcp)");
        close(tcp_socket);
        close(udp_socket);
        exit(-1);
    }
    if (setsockopt(udp_socket, SOL_SOCKET, SO_REUSEADDR,
                (char *) &on, sizeof(on)) < 0) {
        perror("setsockopt() failed (udp)");
        close(tcp_socket);
        close(udp_socket);
        exit(-1);
    }

    if (ioctl(tcp_socket, FIONBIO, (char *) &on) < 0) {
       perror("ioctl() failed (tcp)"); 
       close(tcp_socket);
       close(udp_socket);
       exit(-1);
    }
    if (ioctl(udp_socket, FIONBIO, (char *) &on) < 0) {
       perror("ioctl() failed (udp)"); 
       close(tcp_socket);
       close(udp_socket);
       exit(-1);
    }

    if (listen(tcp_socket, 32) < 0) {
        perror("listen() failed");
        close(tcp_socket);
        close(udp_socket);
        exit(-1);
    }

    // Initialize UDP poll fd
    pool[0].fd = udp_socket;
    pool[0].events = POLLIN;

    // Initialize TCP Listener poll fd
    pool[1].fd = tcp_socket;
    pool[1].events = POLLIN;
    count = 2;

    std::cout << "Running on " << port << " port" <<  std::endl;
}

void sortsum::Server::run() {
    for (;;) {
        std::cout << "Event waiting on poll()" << std::endl;
        wait_ready();
        handle_incoming();
    }
}

void sortsum::Server::wait_ready() {
    if (poll(pool.data(), count, 1000 * 180) < 0) {
        // Close all fd
        perror("poll() failed");
        exit(-1);
    }
}

void sortsum::Server::handle_incoming() {
    int c_size = count;
    std::vector<int> should_close;
    for (int i = 0; i < c_size; ++i) {
        if (pool[i].revents == 0) {
            std::cout << "FD " << pool[i].fd << " no events" << std::endl;
            continue;
        } else if (pool[i].revents & POLLHUP) {
            std::cout << "TCP: Client " << pool[i].fd << " closed the connection" << std::endl;
            should_close.push_back(pool[i].fd);
        } else if (pool[i].fd == tcp_socket && pool[i].revents == POLLIN) {
            std::cout << "TCP: Listening socket is readable" << std::endl;
            for (;;) {
                auto incoming = accept(pool[i].fd, nullptr, nullptr); 
                if (incoming < 0) {
                    std::cout << "TCP: Got all incoming connections" << std::endl;
                    break;
                }
                std::cout << "TCP: New incoming connection = " << incoming << std::endl;
                pool[count].fd = incoming;
                pool[count].events = POLLIN;
                count++;
                std::cout << "Total FD count: " << count << std::endl;
            }
        } else if (pool[i].fd == udp_socket && pool[i].revents == POLLIN) {
            std::cout << "UDP: New incoming request" << std::endl;
            sortsum::UdpSocketStream udp_ss(pool[i].fd, max_buffer);
            std::cout << "UDP: Handling" << std::endl;
            handler(&udp_ss);
            std::cout << "UDP: Handled" << std::endl;
        } else {
            std::cout << "TCP: Handling " << pool[i].fd << std::endl;
            sortsum::TcpSocketStream tcp_ss(pool[i].fd, max_buffer);
            if (!handler(&tcp_ss)) {
                should_close.push_back(pool[i].fd);
            }
            std::cout << "TCP: Handled" << std::endl;
        }
    }
    std::cout << "TCP: Closing" << std::endl;
    for (auto fd: should_close) {
        std::cout << "TCP: fd - " << fd << std::endl;
        close(fd);
        std::remove_if(pool.begin(), pool.end(),
                [&fd](auto poll_fd) {
                    return poll_fd.fd == fd;
                });
        count--;
    }
    std::cout << "TCP: Closed" << std::endl;
}

sortsum::Server::~Server() {
    for (auto fd: pool) {
        close(fd.fd);
    }
}
