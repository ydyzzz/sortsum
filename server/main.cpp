#include "server.h"
#include "handler.h"
#include "socketstream.h"

#include <cstdlib>
#include <functional>
#include <getopt.h>

#include <iostream>

const int pool_size = 200;
const size_t max_buffer = 1024;

int main(int argc, char **argv) {
    int port = 4242;

    static struct option long_option[] = {
        {"port", required_argument, 0, 'p'},
        {0, 0, 0, 0},
    };

    int opt, long_index = 0;
    while ((opt = getopt_long(argc, argv, "p:", long_option, &long_index)) != -1) {
        switch (opt) {
            case 'p':
                port = std::atoi(optarg);
                break;
            default:
                std::cout
                    << argv[0] << ": invalid option -- '" 
                    << optarg << "'" << std::endl;
                std::cout << "Usage: " << argv[0] << " [-p port]" << std::endl;
                exit(-1);
                break;
        }
    }
    sortsum::Server server(port, pool_size, max_buffer, sortsum::Handler());
    server.run();
    return 0;
}
