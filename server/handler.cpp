#include "handler.h"
#include "socketstream.h"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <vector>
#include <sstream>
#include <string>
#include <iostream>

sortsum::Handler::Handler() {}

bool sortsum::Handler::operator()(sortsum::SocketStream *socketstream) {
    std::cout << "Handler: socket -> buffer..." << std::endl;
    std::string line;
    if (!socketstream->read_line(line)) {
        std::cout << "Connection closed" << std::endl;
        return false;
    }
    std::cout << "Handler: buffer: " << line << std::endl;
    std::vector<std::string> tokens;
    std::istringstream iss(line);

    std::copy(std::istream_iterator<std::string>(iss),
            std::istream_iterator<std::string>(),
            std::back_inserter(tokens));

    std::vector<std::string> nums_str;
    std::copy_if(tokens.cbegin(), tokens.cend(), std::back_inserter(nums_str),
            [] (auto entry) {
                return entry.find_first_not_of("0123456789") == std::string::npos;
            }
    );

    std::vector<int> nums;
    std::transform(nums_str.cbegin(), nums_str.cend(), std::back_inserter(nums),
            [] (auto entry) {
                return std::stoi(entry);
            });

    std::sort(nums.begin(), nums.end());
    int sum = std::accumulate(nums.cbegin(), nums.cend(), 0);
    socketstream->write(nums);
    socketstream->write(sum);
    std::cout << "Handler: Done" << std::endl;
    return true;
}
