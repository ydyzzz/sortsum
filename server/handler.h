#pragma once

#include "socketstream.h"

namespace sortsum {
class Handler {
public:
    Handler();
    bool operator()(sortsum::SocketStream *socketstream);
};
};
