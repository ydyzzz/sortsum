#pragma once

#include "socketstream.h"

#include <sys/poll.h>
#include <netinet/in.h>

#include <functional>
#include <vector>

namespace sortsum {
    class Server {
    public:
        Server(int port, size_t size, size_t max_buffer, std::function<bool(sortsum::SocketStream*)>);
        ~Server();
        void wait_ready();
        void handle_incoming();
        void run();
    private:
        size_t max_buffer;
        std::vector<struct pollfd> pool;
        struct sockaddr_in s_addr;
        int count;
        int tcp_socket;
        int udp_socket;
        std::function<bool(sortsum::SocketStream*)> handler;
    };
};
