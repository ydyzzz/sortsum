#pragma once

#include "socketstream.h"

namespace sortsum {

enum class Protocol {
    TCP,
    UDP,
};

class Client {
public:
    explicit Client(size_t max_buffer, const std::string &hostname, sortsum::Protocol protocol, int port);
    void run();
    ~Client();
private:
    size_t max_buffer;
    std::string hostname;
    sortsum::SocketStream *get_socket(sortsum::Protocol, int port);
    sortsum::SocketStream *ss;
    int fd;
};
};
