#include "client.h"

#include <cstdlib>
#include <getopt.h>

#include <iostream>

static const size_t max_buffer = 1024;

int main(int argc, char **argv) {
    int port = 4242;
    std::string hostname("127.0.0.1");
    static struct option long_option[] = {
        {"protocol", required_argument, 0, 'o'},
        {"port", required_argument, 0, 'p'},
        {"hostname", required_argument, 0, 'h'},
        {0, 0, 0, 0},
    };

    auto protocol = sortsum::Protocol::TCP;
    std::string protocol_arg;
    int opt, long_index = 0;
    while ((opt = getopt_long(argc, argv, "po:", long_option, &long_index)) != -1) {
        switch (opt) {
            case 'p':
                port = std::atoi(optarg);
                break;
            case 'o':
                protocol_arg = optarg;
                if (protocol_arg == "tcp") {
                    protocol = sortsum::Protocol::TCP;
                } else if (protocol_arg == "udp") {
                    protocol = sortsum::Protocol::UDP;
                } else {
                    std::cout << argv[0] << ": invalid protocol '" << protocol_arg << "'" << std::endl;
                    std::cout << "Usage: " << argv[0] << " [-p|-protocol port number]" << "[-o|-protocol tcp|udp]" << std::endl;
                }
                break;
            case 'h':
                hostname = optarg;
                break;
            default:
                std::cout
                    << argv[0] << ": invalid option -- '" 
                    << optarg << "'" << std::endl;
                std::cout << "Usage: " << argv[0] << " [-p|-protocol port number]" << "[-o|-protocol tcp|udp]" << std::endl;
                exit(-1);
                break;
        }
    }

    sortsum::Client c(max_buffer, hostname, protocol, port);
    c.run();
    return 0;
}
