#include "client.h"
#include "socketstream.h"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <string>
#include <iostream>

sortsum::Client::Client(size_t max_buffer, const std::string &hostname, sortsum::Protocol protocol, int port)
    : max_buffer(max_buffer), hostname(hostname), ss(get_socket(protocol, port))  {
    }

void sortsum::Client::run() {
    std::string input;
    while (std::getline(std::cin, input) && input.length() > 0) {
        if (!ss->write(input)) {
            std::cout << "Can't send data" << std::endl;
            close(fd);
            exit(-1);
        }
        std::vector<int32_t> result;
        if (!ss->read_vector(result)) {
            std::cout << "¯\\_(ツ)_/¯" << std::endl;
            exit(-1);
        }
        
        // Last element is sum
        auto sum = result.back();
        result.pop_back();
        std::cout << sum << std::endl;

        for (auto num: result) {
            std::cout << num << " ";
        }
        std::cout << std::endl;
    }
}

sortsum::SocketStream  *sortsum::Client::get_socket(
        sortsum::Protocol protocol, int port) {
    sortsum::SocketStream *sstream;
    struct sockaddr_in addr;

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(hostname.c_str());
    addr.sin_port = htons(port);
    switch (protocol) {
        case sortsum::Protocol::TCP:
            fd = socket(AF_INET, SOCK_STREAM, 0);
            if (fd < 0) {
                perror("socket()");
                exit(-1);
            }
            if (connect(fd, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
                perror("connect()");
                close(fd);
                exit(-1);
            }
            sstream = new sortsum::TcpSocketStream(fd, 1024);
            break;
        case sortsum::Protocol::UDP:
            fd = socket(AF_INET, SOCK_DGRAM, 0);
            if (fd < 0) {
                perror("socket()");
                exit(-1);
            }
            sstream = new sortsum::UdpSocketStream(fd, addr, max_buffer);
            break;
    }
    return sstream;
}

sortsum::Client::~Client() {
    close(fd);
    delete ss;
}
