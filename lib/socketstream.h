#pragma once

#include <netinet/in.h>
#include <cstddef>
#include <stdint.h>

#include <vector>
#include <string>

namespace sortsum {

class SocketStream {
public:
    explicit SocketStream(int fd, size_t max_buffer);

    virtual int write(const void *buf, size_t sz) = 0;
    void write(int32_t n);
    void write(const std::vector<int32_t> &data);
    bool write(const std::string &line);

    virtual int read(void *buf, size_t sz) = 0;
    bool read_line(std::string &line);
    bool read_vector(std::vector<int32_t> &v);

    int flush();

    virtual ~SocketStream();
protected:
    int fd;
    size_t max_buffer;
    std::vector<int32_t> buffer;
};

class TcpSocketStream : public SocketStream {
public:
    explicit TcpSocketStream(int fd, size_t max_buffer);
    int write(const void *buf, size_t sz);
    int read(void *buf, size_t sz);
    ~TcpSocketStream();
};

class UdpSocketStream : public SocketStream {
public:
    explicit UdpSocketStream(int fd, size_t max_buffer);
    explicit UdpSocketStream(int fd, struct sockaddr_in addr, size_t max_buffer);
    int write(const void *buf, size_t sz);
    int read(void *buf, size_t sz);
    ~UdpSocketStream();
private:
    struct sockaddr_in addr;
    socklen_t len;
};
};
