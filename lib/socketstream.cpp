#include "socketstream.h"

#include <algorithm>

#include <iterator>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include <iostream>

sortsum::SocketStream::SocketStream(int fd, size_t max_buffer)
    : fd(fd), max_buffer(max_buffer) {}

void sortsum::SocketStream::write(int32_t n) {
    buffer.push_back(htonl(n));
}

void sortsum::SocketStream::write(const std::vector<int32_t> &data) {
    for (auto n: data) {
        write(n);
    }
}

bool sortsum::SocketStream::write(const std::string &line) {
    return write(line.c_str(), std::min(line.size(), max_buffer)) > 0;
}

bool sortsum::SocketStream::read_line(std::string &line) {
    char *buffer = new char[max_buffer];
    int n = read(buffer, max_buffer);
    if (n < 0) {
        perror("read()");
        delete []buffer;
        return false;
    }
    buffer[n] = '\0';
    line.clear();
    line.append(buffer, n);
    delete []buffer;
    return true;
}

bool sortsum::SocketStream::read_vector(std::vector<int32_t> &v) {
    char *buffer = new char[max_buffer];
    int c = read(buffer, max_buffer);
    if (c < 0) {
        perror("read()");
        delete []buffer;
        return false;
    }
    if (c > 0) {
        int num_count = c / sizeof(int32_t);
        int32_t *nums = reinterpret_cast<int32_t*>(buffer);
        std::transform(nums, nums + num_count, std::back_inserter(v),
                [] (auto network_num) {
                    return ntohl(network_num);
                });
    }
    delete []buffer;
    return true;
}

int sortsum::SocketStream::flush() {
    size_t bytes = std::min(sizeof(buffer[0]) * buffer.size(), max_buffer);
    if (bytes == 0) {
        return 0;
    }
    auto ret = write(buffer.data(), bytes);
    buffer.clear();
    return ret;
}

sortsum::SocketStream::~SocketStream() { }

sortsum::TcpSocketStream::TcpSocketStream(int fd, size_t max_buffer)
    : sortsum::SocketStream(fd, max_buffer) {}

int sortsum::TcpSocketStream::write(const void *buf, size_t sz) {
    return send(fd, buf, sz, 0);
}

int sortsum::TcpSocketStream::read(void *buf, size_t sz) {
    return recv(fd, buf, sz, 0);
}

sortsum::TcpSocketStream::~TcpSocketStream() {
    flush();
}

sortsum::UdpSocketStream::UdpSocketStream(int fd, size_t max_buffer)
    : sortsum::SocketStream(fd, max_buffer) {
    memset(&addr, 0, sizeof(addr));
}

sortsum::UdpSocketStream::UdpSocketStream(int fd, struct sockaddr_in addr, size_t max_buffer)
    : sortsum::SocketStream(fd, max_buffer), addr(addr) {}

int sortsum::UdpSocketStream::write(const void *buf, size_t sz) {
    len = sizeof(addr);
    return sendto(fd, buf, sz, 0, (const struct sockaddr *) &addr, len);
}

int sortsum::UdpSocketStream::read(void *buf, size_t sz) {
    len = sizeof(addr);
    return recvfrom(fd, buf, sz, 0, (struct sockaddr *) &addr, &len);

}

sortsum::UdpSocketStream::~UdpSocketStream() {
    flush();
}
