# Sortsum
___
Sortsum is TCP/UDP client & server app. The client sends ASCII text and receives int32 sorted numbers and theirs sum from the text. 

## Building
___
```bash
mkdir build
cd build
cmake ..
cmake --build .
```

## Usage
### Server
___
```bash
sortsum_server --port [port(default=4242)]
```
### Client
___
```bash
sortsum_client --port [port(default=4242)] --protocol [tcp|udp(default=tcp)] --hostname [hostname(default=127.0.0.1)]
```

## Example
___
```bash
sortsum_server --port 4343
```
___
```bash
sortsum_client --port 4343 --protocol udp
```
```
< hello 21 1 3 5 world
> 30
> 1 3 5 21
```
